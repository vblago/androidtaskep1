package ltd.vblago.taskep1.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import ltd.vblago.taskep1.R;
import ltd.vblago.taskep1.fragment.AddContactFragment;
import ltd.vblago.taskep1.fragment.DetailFragment;
import ltd.vblago.taskep1.fragment.ListFragment;
import ltd.vblago.taskep1.model.Number;
import ltd.vblago.taskep1.util.ActivityCommunication;
import ltd.vblago.taskep1.util.Generator;

public class MainActivity extends AppCompatActivity
        implements ActivityCommunication{

    ArrayList<Number> numbers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numbers = Generator.getNumbers();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ListFragment.newInstance())
                .commit();
    }

    @Override
    public void goToDetailFragment(Number number) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, DetailFragment.newInstance(number))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void goToAddContactFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, AddContactFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    public void goToListFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ListFragment.newInstance())
                .commit();
    }

    @Override
    public ArrayList<Number> getNumbers() {
        return numbers;
    }

    @Override
    public void addNumber(Number number) {
        numbers.add(number);
        goToListFragment();
    }

}

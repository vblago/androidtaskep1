package ltd.vblago.taskep1.util;

import java.util.ArrayList;

import ltd.vblago.taskep1.model.Number;

public interface ActivityCommunication {
    void goToDetailFragment(Number number);
    void goToAddContactFragment();
    ArrayList<Number> getNumbers();
    void addNumber(Number number);
}

package ltd.vblago.taskep1.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ltd.vblago.taskep1.R;
import ltd.vblago.taskep1.model.Number;

public class NumberAdapter extends BaseAdapter {
    private Context context;
    private List<Number> list;

    public NumberAdapter(Context context, List<Number> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Number getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View root = convertView;

        if (root == null) {
            root = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
            viewHolder = new ViewHolder(root);
            root.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) root.getTag();
        }

        Number number = getItem(position);
        viewHolder.imageView.setImageResource(number.getImage());
        viewHolder.nameView.setText(number.getName());
        viewHolder.emailView.setText(number.getEmail());
        return root;
    }

    static class ViewHolder{
        @BindView(R.id.name_view) TextView nameView;
        @BindView(R.id.image_view) ImageView imageView;
        @BindView(R.id.email_view) TextView emailView;

        ViewHolder(View root) {
            ButterKnife.bind(this, root);
        }
    }
}
